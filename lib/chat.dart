import 'package:flutter/material.dart';
import 'drawer.dart';

class Chat extends StatelessWidget {
  final title = 'Chat';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      drawer: AppDrawer(),
    );
  }
}

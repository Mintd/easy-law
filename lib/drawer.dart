import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Easy Law'),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          Container(
            color: ModalRoute.of(context).settings.name == '/'
                ? Colors.blue
                : null,
            child: ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {
                Navigator.popUntil(context, ModalRoute.withName('/'));
              },
            ),
          ),
          Container(
            color: ModalRoute.of(context).settings.name == '/search'
                ? Colors.blue
                : null,
            child: ListTile(
              leading: Icon(Icons.search),
              title: Text('Search/Categories'),
              onTap: () {
                Navigator.pop(context);

                if (ModalRoute.of(context).settings.name != '/search') {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                  Navigator.pushNamed(
                    context,
                    '/search',
                  );
                }
              },
            ),
          ),
          Container(
            color: ModalRoute.of(context).settings.name == '/chat'
                ? Colors.blue
                : null,
            child: ListTile(
              leading: Icon(Icons.chat_bubble),
              title: Text('Chat'),
              onTap: () {
                Navigator.pop(context);

                if (ModalRoute.of(context).settings.name != '/chat') {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                  Navigator.pushNamed(
                    context,
                    '/chat',
                  );
                }
              },
            ),
          ),
          Container(
            color: ModalRoute.of(context).settings.name == '/settings'
                ? Colors.blue
                : null,
            child: ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.pop(context);

                if (ModalRoute.of(context).settings.name != '/settings') {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                  Navigator.pushNamed(
                    context,
                    '/settings',
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

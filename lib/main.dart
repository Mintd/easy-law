import 'package:flutter/material.dart';
import 'home.dart';
import 'search.dart';
import 'chat.dart';
import 'settings.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final name = 'Easy Law';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: name,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Home(title: name),
      routes: <String, WidgetBuilder>{
        '/search': (BuildContext context) => SearchCategory(),
        '/chat': (BuildContext context) => Chat(),
        '/settings': (BuildContext context) => Settings(),
      },
    );
  }
}

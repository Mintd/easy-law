import 'package:flutter/material.dart';
import 'drawer.dart';

class Settings extends StatelessWidget {
  final title = 'Settings';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      drawer: AppDrawer(),
    );
  }
}

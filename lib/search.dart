import 'package:flutter/material.dart';
import 'drawer.dart';

class SearchCategory extends StatelessWidget {
  final title = 'Search/Category';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      drawer: AppDrawer(),
    );
  }
}
